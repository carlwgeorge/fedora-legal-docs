////

Copyright Fedora Project Authors.

SPDX-License-Identifier: CC-BY-SA-4.0

////

= Fedora Export Control Policy

By contributing to or downloading Fedora content, you acknowledge that you understand all of the following: Fedora software and technical information may be subject to the U.S. Export Administration Regulations (the “EAR”) and other U.S. and foreign laws and may not be exported, re-exported or transferred: 

(a) to a prohibited destination country under the EAR and the U.S. Department of Treasury, Office of Foreign Assets Controls (currently Cuba, Iran, North Korea, Syria, and the Crimea Region of Ukraine, subject to change as posted by the United States government); 

(b) to any prohibited destination or to any end user who has been prohibited from participating in U.S. export transactions by any federal agency of the U.S. government; or 

(c) for use in connection with the design, development or production of nuclear, chemical or biological weapons, or rocket systems, space launch vehicles, or sounding rockets, or unmanned air vehicle systems. 

You may not download Fedora software or technical information if you are located in one of these countries or otherwise subject to these restrictions. You may not provide Fedora software or technical information to individuals or entities located in one of these countries or otherwise subject to these restrictions. You are also responsible for compliance with foreign law requirements applicable to the import, export and use of Fedora software and technical information.

Fedora software in source code and binary code form are publicly available and are not subject to the EAR in accordance with §742.15(b).
