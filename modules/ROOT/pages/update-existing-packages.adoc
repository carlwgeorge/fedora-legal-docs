////

Copyright Fedora Project Authors.

SPDX-License-Identifier: CC-BY-SA-4.0

////

= Updating License: field for Existing Packages

In July 2022 Fedora adopted the use of SPDX license expressions in the `License:` field of the package spec file. While this change is relatively easy to manage for new packages added to Fedora, there remains the larger task of how to efficiently update the `License:` field for all existing spec files. 

This page addresses some relevant background information and addresses some considerations for that task. This page will be updated to reflect that process as time goes on.

== Background: Mapping Callaway short names to SPDX identifiers

Members of SPDX-legal have already undertaken a comparison of all Fedora "good" and "bad" license texts in order to map Callaway short names to SPDX license identifiers to the extent possible. 

=== Process used

This mapping was captured initially in a spreadsheet containing a list of all Fedora licenses from the wiki and included the following steps and guidelines:

* The text of every Fedora license ("good", "good for fonts", "bad" etc.) was analyzed to determine if it matched to a license on the SPDX License List (mostly using SPDX license-diff tool, plus general knowledge of the SPDX-legal community)

** If there was a match, that SPDX identifer was captured in the mapping

* For all "good" licenses: a search for the Callaway short name in the `License:` field of all spec files in Fedora Rawhide was conducted (mostly using the Sourcegraph code search function)

** If the Callaway short name was found in Fedora Rawhide AND there was a match to an SPDX license identifier, then the SPDX identifier was captured in the mapping

** If the Callaway short name was found in Fedora Rawhide AND there was no match to an SPDX license identifier, then the Fedora "good" license was submitted to SPDX for inclusion. Due to the nature of https://github.com/spdx/license-list-XML/blob/master/DOCS/license-inclusion-principles.md[SPDX License List inclusion principles], all Fedora "good" licenses submitted in this process were accepted into the SPDX License List.

** If the Callaway short name was NOT found in Fedora Rawhide AND there was no match to an SPDX license identifier, then the Fedora "good" license was given a `LicenseRef-` SPDX identifier and captured in the mapping. The rationale was that Fedora "good" licenses that are no longer present in Fedora don't need an SPDX identifier, so there was no reason to submit them to the SPDX License List.

* For all "bad" licenses: if there was an identifiable license text in the Fedora wiki (either a functioning link or captured in the Fedora wiki) AND there was no corresponding SPDX identifier, then the the Fedora "bad" license was given a `LicenseRef-` SPDX identifier and captured in the mapping. If a "bad" license did match to an SPDX license, then that SPDX identifer was captured in the mapping.

An overarching guideline during this mapping process was to not submit Fedora "bad" licenses or Fedora "good" licenses that are no longer used in Fedora Linux to the SPDX License List. The rationale was that there is no reason to burden the SPDX-legal team about licenses that are not allowed in Fedora or no longer used. 

If such a license is later found in Fedora Linux and determined to be allowed, it can then be submitted to the SPDX License List.

This mapping data was then converted into the https://gitlab.com/fedora/legal/fedora-license-data[Fedora License Data TOML file format].

== Changelog entry

Before we dive into different cases - when you migrate your existing package to SPDX identifier, please add this line to `%changelog` section:

----
- migrated to SPDX license
----

or something similar. The important part is "spdx" in changelog entry. This will helps the legal-team audit packages.

== Updating existing packages: One-to-one ids

Many of the Callaway short names map one-to-one to SPDX identifiers, meaning that each the Callaway short name and the SPDX identifier represent a specific license. (For convenience, we are assuming here that the Callaway short names  applied an implicit matching criterion to some degree.) In some cases the Callaway short name and SPDX identifier are different and in some cases they are the same or very similar. For example:

* Apache License 2.0: Callaway short name = `ASL 2.0`, SPDX identifier = `Apache-2.0`
* Beerware License: Callaway short name = `Beerware`, SPDX identifier = `Beerware`
* GPLv2 only: Callaway short name = `GPLv2`, SPDX identifier = `GPL-2.0-only`
* GPLv2-or-later: Callaway short name = `GPLv2+`, SPDX identifier = `GPL-2.0-or-later`
* Open Software License 3.0: Callaway short name = `OSL 3.0`, SPDX identifier = `OSL-3.0`

In these cases, the Callaway short names could potentially be updated programmatically (see `license-fedora2spdx` tool) with the corresponding SPDX identifier. However, a very careful mapping would need to be implemented to ensure such replacements were not over-inclusive of Callaway short names that may apply to more than one license and SPDX identifier, as explained below.

Also, it is important to note that such programmatic updating would only serve as an initial step to making the `License:` field conform to the xref:license-field.adoc[new guidelines]. For example, under the Callaway system, some Fedora package maintainers engaged in some form of "effective license" analysis in deciding how to populate the `License:` field. The new Fedora guidelines on populating the `License:` field reject the use of effective license analysis and aim at a more precise enumeration of the various licenses covering the relevant portions of the package source code. Even apart from the issue of Callaway umbrella short names (discussed below), the Callaway short names were generally not applied with that sort of precision. 

Package maintainers are welcome to start updating the `License:` field for their packages, given the above considerations.

== Updating existing packages: Callaway short name categories

The SPDX License List has an established set of https://spdx.github.io/spdx-spec/v2.3/license-matching-guidelines-and-templates/[matching guidelines] that define what constitutes a match to a license or exception on the SPDX License List. This ensures that SPDX license identifiers mean the same thing wherever they are used and takes a somewhat conservative approach as to what is substantively different. Each identifier for a license or an exception on the SPDX License List represents a single template text for that license or exception.

In contrast, some Callaway short names are umbrella categories referring to a set of different license texts that were treated as equivalent for Fedora package metadata purposes. This means, there are Callaway short names that correspond to multiple SPDX ids.  Some of the various license texts that correspond to a Callaway category short name were captured in the mapping and some were not. In any case, review of the
actual text of the license for packages using these Callaway short names will be needed to determine what SPDX id applies; and in some cases, the licenses will need to be added to the Fedora License Data. 

The Callaway category short names are listed below, along with an estimate (where possible) of how many times this short name appears in Fedora spec files according to Sourcegraph on July 25, 2022 (note: due to composite license expressions, the number of times one Callaway short name appears might overlap with another). 

Advice for package maintainers is provided below for the specific Callaway category short names.  

==== Bulk review

In the case of some Callaway category short names (as may be noted below), it may be beneficial to analyze all packages in some form of bulk way, as described in more detail below. This may give insight into:

* How many different licenses (according to the SPDX matching guidelines) the Callaway short name categories actually represent
* How many of these are already on the SPDX License List and need to be updated or added to the Fedora License Data
* How many will need to be submitted for inclusion to the SPDX License List and updated or added to the Fedora License Data

A bulk review will  allow the SPDX-legal team to determine the most efficient and sensible way to address a potentially large number of new submissions to the SPDX License List.

=== `MIT`
How used in Fedora: to indicate a large number of short permissive licenses in the Callaway system having some textual similarity or historical connecton to the SPDX `MIT` license. Some, but not all, were captured on https://fedoraproject.org/wiki/Licensing:MIT?rd=Licensing/ 

SPDX coverage: The SPDX License List recognizes `MIT` as a specific license, as well as a fair number of MIT variants, some of which come from the Fedora "good" list and above link. Further inspection of the actual license text in Fedora packages that use the `MIT` Callaway short name will likely turn up new variants that will need to be added to the Fedora License Data and submitted to the SPDX License List.

Package Maintainers: Find the actual license in the source file and review. If the actual license text matches to a Fedora allowed license with an SPDX identifer, then update the `License:` field accordingly. If not, submit xref:license-review-process.adoc[for license review]

* Sourcegraph shows over 500 packages

=== `BSD` 

How used in Fedora: to indicate a large number of short permissive licenses bearing some textual similarity or historical connection to licenses in the BSD family.

SPDX coverage: The SPDX License List recognizes a fair number of licenses in the BSD family, e.g., BSD-2-Clause, BSD-3-Clause, etc. Note there is no SPDX license id that is simply `BSD`. 

Package Maintainers: Find the actual license in the source file and review. If the actual license text matches to a Fedora allowed license with an SPDX identifer, then update the `License:` field accordingly. If not, submit xref:license-review-process.adoc[for license review]

* Sourcegraph shows over 500 packages

=== `with exceptions`

How used in Fedora: to indicate GPL or LGPL with an additional permission. Because the text for most exceptions in Fedora Linux was not captured in the previous licensing wiki, it's likely that many new SPDX expressions will need to be added to the Fedora license data once these packages are investigated more closely to identify the exact exception text.

SPDX coverage: The text for only couple of specific exceptions were captured in the previous Fedora licensing wiki, and those have been added as an specific SPDX expression in the Fedora License Data. The SPDX License List includes many known exceptions, including those normally used with the GPL family of licenses. 

Package maintainers: Find the actual exception text in the source file and review. If the actual license text matches an SPDX expression already captured in the Fedora License Data, then update the `License:` field accordingly. If not, submit xref:license-review-process.adoc[for license review]

* Sourcegraph shows https://sourcegraph.com/search?q=context:global+r:src.fedoraproject.org+file:.*%5C.spec%24+%5ELicense:%5Cs%2B.*with+exceptions+TIMEOUT:120s&patternType=regexp[approximately 335 packages]

=== `LGPLv2` and `LGPLv2+`

How used in Fedora & SPDX coverage: 

* LGPLv2 - used to represent both `LGPL-2.0-only` and `LGPL-2.1-only`
* LGPLv2+ - used to represent both `LGPL-2.0-or-later` and `LGPL-2.1-or-later`

Package maintainers: Update `License:` field according to which LGPL version is relevant upon inspection of license notices in source files.

=== `Public Domain` 

How used in Fedora: under the Callaway system, this short name was used as follows: "Works which are clearly marked as being in the Public Domain, and for which no evidence is known to contradict this statement, are treated in Fedora as being in the Public Domain, on the grounds that the intentions of the original creator are reflected by such a use, even if due to regional issues, it may not have been possible for the original creator to fully abandon all of their their copyrights on the work and place it fully into the Public Domain. If you believe that a work in Fedora which is marked as being in the Public Domain is actually available under a copyright license, please inform us of this fact with details, and we will immediately investigate the claim."

SPDX coverage: While SPDX has captured some specific public domain dedications, it is likely that very few of the public domain dedication texts referenced by the Callaway short name "Public Domain" are mapped due to its category nature.

Special considerations: Given the number of packages that use this Callaway category short name, it's impossible to tell how many unique public domain dedications this represents. This is a case where it may be preferable implement a xref:#bulk_review[bulk review] to collect the text for these public domain dedications in bulk and then discuss collaboratively with the SPDX community as to how to deal with them. 

Package Maintainers: Locate the actual text comprising the public domain statement in the source files. If it turns out the actual license text matches an SPDX expression already captured in the Fedora License Data, then update the `License:` field accordingly. If not, submit a https://gitlab.com/fedora/legal/fedora-license-data/-/issues/new[review issue] and record this text by way of merge request to https://gitlab.com/fedora/legal/fedora-license-data/-/blob/main/public-domain-text.txt
If the MR is accepted, then update the `License: field` with a placeholder SPDX id of `LicenseRef-Fedora-Public-Domain`. 
If it turns out the actual text is not a public domain dedication and needs different treatment, instructions will be provided in the comments of the corresponding issue.

We may later do some form analysis of the collected text, which may inform further updates to the SPDX identifier or `License:` field. 

** Sourcegraph shows https://sourcegraph.com/search?q=context:global+r:src.fedoraproject.org+file:.*%5C.spec%24+%5ELicense:%5Cs%2B.*public+domain+TIMEOUT:120s&patternType=regexp[450 packages]

=== `Copyright only`

How used in Fedora: under the Callaway system, this short name was used for very short licenses that only required that the copyright notice be retained. 

SPDX coverage: Unknown, as it is not known what kinds of license texts this includes and how different they might be from the other categories.

Package Maintainers: Review license texts in the source code of their packages. If the actual license text matches to a Fedora allowed license with an SPDX identifer, then update the `License:` field accordingly. If not, submit xref:license-review-process.adoc[for license review].
 
* Sourcegraph shows https://sourcegraph.com/search?q=context:global+r:src.fedoraproject.org+file:.*%5C.spec%24+%5ELicense:%5Cs%2B.*copyright+TIMEOUT:120s&patternType=regexp[31 packages]

=== `Freely redistributable without restrictions`

How used in Fedora: This appears to be a very old short name that has not been used for some time and was deprecated in the previous packaging guidelines, but has persisted in some packages nonetheless.

Package Maintainers: (similar approach as used with Public Domain) If it turns out the actual license text matches an SPDX expression already captured in the Fedora License Data, then update the `License:` field accordingly. If not, submit a https://gitlab.com/fedora/legal/fedora-license-data/-/issues/new[review issue] and record this text by way of merge request to https://gitlab.com/fedora/legal/fedora-license-data/-/blob/main/UltraPermissive.txt
If the MR is accepted, then update the `License: field` with a placeholder SPDX id of `LicenseRef-Fedora-UltraPermissive`. 
If it turns out the actual text needs different treatment, instructions will be provided in the comments of the corresponding issue.

* Sourcegraph shows https://sourcegraph.com/search?q=context:global+r:src.fedoraproject.org+file:.*%5C.spec%24+%5ELicense:%5Cs%2B.*freely+redistributable+TIMEOUT:120s&patternType=regexp[approx 22 packages]

=== `Redistributable, no modification permitted`

How used in Fedora: Under the legacy Callaway system, the `License:` field for Fedora packages containing firmware under non-FOSS licenses mostly used the tag `Redistributable, no modification permitted`. There may also be some non-firmware packages that used this short name. In any case, because no specific license texts for these licenses were captured on the Fedora wiki, we don't know what specifics.

SPDX coverage: Unknown, unlikely to have much coverage because some of the restrictions that Fedora allows for a license to be [allowed for firmware] will not meet the SPDX License Inclusion Principles. This is a situation where collecting the actual texts of the licenses that use this Callaway short name and then collaborating with SPDX would be a good approach.

Package Maintainers: Review license texts in the source code. In the unlikely case that the actual license text happens to match an existing Fedora allowed license with an SPDX identifer, then update the License: field accordingly. Otherwise, submit for license review.  Firmware licenses will be reviewed under the xref:license-approval.adoc#_allowed_for_firmware[allowed for firmware] criteria. For non-firmware packages, other appropriate Fedora approval criteria will apply. Treatment as to SPDX submission and identifiers may be delayed until we have a more complete picture of the various license that use this Callaway short name, as noted above.  

* Sourcegraph shows https://sourcegraph.com/search?q=context:global+r:src.fedoraproject.org+file:.*%5C.spec%24+%5ELicense:%5Cs%2B.*redistributable%2C+no+modification+TIMEOUT:120s&patternType=regexp[approximately 50 packages]

=== Small category short names
The following Callaway short names are known to represent one or more SPDX licenses, most of which are likely already to be represented with an SPDX id and in the Fedora License Data (and if not... then submit!). In all cases, the actual license text will need to be found to determine the correct SPDX id to use. 

NOTE: there may be more small category Callaway short names besides the ones listed here!

* *MIT with advertising* - used to represent a couple of different license texts
** Sourcegraph shows https://sourcegraph.com/search?q=context:global+r:src.fedoraproject.org+file:.*%5C.spec%24+%5ELicense:%5Cs%2B.*MIT+with+advertising+TIMEOUT:120s&patternType=regexp[approx 8 packages]

* *BSD with advertising* - used to represent a few different license texts
** Sourcegraph shows https://sourcegraph.com/search?q=context:global+r:src.fedoraproject.org+file:.*%5C.spec%24+%5ELicense:%5Cs%2B.*BSD+with+advertising+TIMEOUT:120s&patternType=regexp[approx 54 packages]

* *MPLv1.0* - used to represent two different license texts (could also be Freeimage Public License)
** Sourcegraph shows https://sourcegraph.com/search?q=context:global+r:src.fedoraproject.org+file:.*%5C.spec%24+%5ELicense:%5Cs%2B.*MPLv1.0+TIMEOUT:120s&patternType=regexp[6 packages]

* *MPLv1.1* - used to represent a few different license texts (could also be CUA Office Public License Version 1.0)
** Sourcegraph shows https://sourcegraph.com/search?q=context:global+r:src.fedoraproject.org+file:.*%5C.spec%24+%5ELicense:%5Cs%2B.*MPLv1.1+TIMEOUT:120s&patternType=regexp[105 packages]

* *CECILL* - used to represent at least two different licenses
** Sourcegraphs shows https://sourcegraph.com/search?q=context:global+r:src.fedoraproject.org+file:.*%5C.spec%24+%5ELicense:%5Cs%2B.*cecill+TIMEOUT:120s&patternType=regexp[approx 11 packages using just "CECILL"]

* *HSRL* - used to represent a few different licenses
** Sourcegraph shows https://sourcegraph.com/search?q=context:global+r:src.fedoraproject.org+file:.*%5C.spec%24+%5ELicense:%5Cs%2B.*HSRL+TIMEOUT:120s&patternType=regexp[2 packages]

* *Python* and *AFL* and possibly others - single Fedora short name used to cover multiple versions of the license







